<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{

    public function index()
    {
        $data['page'] = 'index';
        $data['posts'] = Post::orderBy('id', 'DESC')->paginate(10);
        return view('post.index', $data);
    }

    public function create() 
    {
        $data['page'] = 'create';
        return view('post.index', $data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title'  => 'required|max:255',
            'status' => 'required|in:Active,Inactive',
        ]);

        $image = '';
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $image = time() . '_post_' . $file->getClientOriginalName();
            $file->move('image/', $image);
        }

        Post::create([
            'title' => $request->title,
            'sort_description' => $request->sort_description,
            'description' => $request->description,
            'status' => $request->status,
            'image' => $image,
        ]);

        return redirect()->back()->with('success', 'Post save successfully');

    }

    public function edit($id) 
    {
        $data['page'] = 'edit';
        $data['post'] = Post::find($id);
        
        return view('post.index', $data);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'  => 'required|max:255',
            'status' => 'required|in:Active,Inactive',
        ]);

        $post = Post::find($id);

        if(isset($post)) {
            $image = $post->image;
            if ($request->hasFile('image')) {
                if ($post->image){
                    unlink(public_path('/image/').$post->image);
                }
                $file = $request->file('image');
                $image = time() . '_post_' . $file->getClientOriginalName();
                $file->move('image/', $image);
            }
    
            $post->update([
                'title' => $request->title,
                'sort_description' => $request->sort_description,
                'description' => $request->description,
                'status' => $request->status,
                'image' => $image,
            ]);
    
            return redirect()->back()->with('success', 'Post update successfully');
        } else {
            return redirect()->back()->with('error', 'Somethng went wrong. Please try agin.');
        }
    }

    public function show($id) 
    {
        $data['page'] = 'show';
        $data['post'] = Post::find($id);
        
        return view('post.index', $data);
    }

    public function destroy($id) 
    {
        $post = Post::find($id);
        
        if(isset($post)) {
            if ($post->image){
                unlink(public_path('/image/').$post->image);
            }
            $post->delete();
            return redirect()->back()->with('success', 'Post delete successfully');
        } else {
            return redirect()->back()->with('error', 'Somethng went wrong. Please try agin.');
        }
    }





}
