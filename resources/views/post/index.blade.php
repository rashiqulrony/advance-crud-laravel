@extends('layouts.post')

@section('title')
    Post
@endsection

@section('content')
    <div class="container">
    
        <div class="row d-flex justify-content-center">
            <h1>Simple CRUD with Laravel</h1>

            @if (session('success') || session('error'))
                <div class="col-md-10 mt-5">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @elseif(session('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif
                </div>
            @endif
            @if($page == 'index')
                <div class="col-md-10 mt-5">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{ route('post.create') }}" class="right">Create New</a>
                        </div>
                        <div class="card-body">
                            <table class="table table-border table-hover">
                                <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Title</th>
                                    <th>Short Description</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($posts) > 0 && isset($posts))
                                    @foreach($posts as $key => $post)
                                        <tr>
                                            <td>{{ ++$key }}</td>
                                            <td>{{ $post->title }}</td>
                                            <td>{{ $post->sort_description }}</td>
                                            <td>{{ $post->status }}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Action
                                                    </button>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item" href="{{ route('post.edit', $post->id) }}">Edit</a>
                                                        <a class="dropdown-item" href="{{ route('post.show', $post->id) }}">View</a>
                                                        <a class="dropdown-item" onclick="deletePost(`{{ route('post.destroy', $post->id) }}`)" href="#">Delete</a>

                                                
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" align="center">No data found</td>
                                    </tr>
                                @endif
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td class="text-center" colspan="5" align="center">
                                            {{ $posts->links() }}
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            @elseif($page == 'create' || $page == 'edit')
                <div class="col-md-10 mt-5">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{ route('post.index') }}" class="right">Back To list</a>
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{ $page == 'create' ? route('post.store') : route('post.update', $post->id) }}" enctype="multipart/form-data">
                                @csrf
                                @if($page == 'edit')
                                    @method('PUT')
                                @endif
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" class="form-control" value="{{ old('title', isset($post) ? $post->title : '') }}" name="title" id="title" placeholder="Title">
                                    @error('title')
                                      <span class="text-danger">
                                          <strong>{{ $message }}</strong>
                                      </span>
                                    @enderror
                                  </div>
                                  <div class="form-group">
                                      <label for="short_description">Shot Description</label>
                                      <textarea type="text" class="form-control" name="sort_description" id="sort_description" placeholder="Shot Description">{{ old('sort_description', isset($post) ? $post->sort_description : '') }}</textarea>
                                      @error('short_description')
                                      <span class="text-danger">
                                          <strong>{{ $message }}</strong>
                                      </span>
                                      @enderror
                                  </div>

                                  <div class="form-group">
                                      <label for="description">Description</label>
                                      <textarea type="text" rows="10" class="form-control" name="description" id="description" placeholder="Description">{{ old('description', isset($post) ? $post->description : '') }}</textarea>
                                      @error('description')
                                      <span class="text-danger">
                                          <strong>{{ $message }}</strong>
                                      </span>
                                      @enderror
                                  </div>

                                  <div class="form-group">
                                      <label for="image">Image</label>
                                      <input type="file" accept="image/*" class="form-control" name="image" id="image">
                                      @error('image')
                                      <span class="text-danger">
                                          <strong>{{ $message }}</strong>
                                      </span>
                                      @enderror
                                  </div>

                                  <div class="form-group">
                                      <label for="status">Status</label>
                                      <select class="form-control" name="status" id="status">
                                          <option selected disabled hidden>Select Status</option>
                                          <option value="Active" {{ isset($post) && $post->status == 'Active' ? 'selected' : '' }}>Active</option>
                                          <option value="Inactive" {{ isset($post) && $post->status == 'Inactive' ? 'selected' : '' }}>Inactive</option>
                                      </select>
                                      @error('status')
                                      <span class="text-danger">
                                          <strong>{{ $message }}</strong>
                                      </span>
                                      @enderror
                                  </div>
                                  
                                  <button type="submit" class="btn btn-primary">Submit</button>

                            </form>
                        </div>
                    </div>
                </div>

            @elseif($page == 'show')
            <div class="col-md-10 mt-5">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('post.index') }}" class="right">Back To list</a>
                    </div>
                    <div class="card-body">
                        <table class="table table-border table-hover">
                            <tr>
                                <th>Title</th>
                                <td>{{ $post->title }}</td>
                            </tr>
                            <tr>
                                <th>Sort Description</th>
                                <td>{{ $post->sort_description }}</td>
                            </tr>
                            <tr>
                                <th>Description</th>
                                <td>{{ $post->description }}</td>
                            </tr>

                            <tr>
                                <th>Status</th>
                                <td>{{ $post->status }}</td>
                            </tr>
                            <tr>
                                <th>Image</th>
                                <td> 
                                    @if($post->image)
                                        <img width="30%" src="{{ asset('image/'. $post->image) }}" alt="{{ $post->title }}">
                                    @else
                                        <small class="text-danger">Image not found</small>
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            @endif


        </div>
    </div>


    @if($page == 'index')
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              Are you sure delete this post ??
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" onclick="event.preventDefault();document.getElementById('deleteForm').submit();" class="btn btn-danger">Delete</button>

              <form id="deleteForm" action="" method="POST" class="d-none">
                @csrf
                @method('DELETE')
              </form>               
            </div>
          </div>
        </div>
      </div>
      <script>
          function deletePost(url) {
            $('#deleteForm').attr('action', url);
            $('#deleteModal').modal('show');
          }
      </script>
    @endif
    
@endsection
